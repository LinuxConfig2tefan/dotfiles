# dotfiles 🐧

This repository includes my dotfiles. I use [chezmoi](https://www.chezmoi.io/) to manage them.

## Install ⚙️

Install `chezmoi` for your Linux distrobution and use the following command:

```sh
chezmoi init git@gitlab.com:2tefan-projects/Linux/dotfiles.git
```

## Current Setup ✨

- [Commit](https://gitlab.com/LinuxConfig2tefan/dotfiles/-/tree/d0e728feea77830e425a51ae8eee7ba27ee4ff09)
- 2022-08-05

My setup may be subject to change in the future but currently my setup looks like this:

![Image of i3 as WM some terminals open](https://gitlab.com/2tefan-projects/Linux/screenshots/-/raw/2f67797fb6ed8a945fa1ebe303d2cccb5905271c/2022-08-05_i3_arch_768p.png)

Highlights
- [i3](https://i3wm.org/) (= ❤️) *(with [gaps](https://github.com/Airblader/i3))*
- [zsh](https://www.zsh.org/) 🖥️
- [alacritty](https://alacritty.org/) 🖋️
- [dunst](https://dunst-project.org/) 📨
- [polybar](https://polybar.github.io/) 🛐


## History 📖

Here is not much to see (yet). If I ever plan to completely change my config up or at least make significant changes, then the current version will come down here and over time an overview over the development should be seen here...

In the meantime, some background information: I started out with Arch w/ XFCE, but over time I switched to i3. But it took a long time until I switched to non-XFCE programs, as I continued to use the notify daemon or the terminal from XFCE. The only thing I still use is the power manager for my laptop running Manjaro.

If you have read until here, you deserve a cookie: 🍪 Thanks for reading and have a great day 😊
