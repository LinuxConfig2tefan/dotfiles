-- Set some keybindings for nvim
local wk = require("which-key")
-- As an example, we will create the following mappings:
--  * <leader>ff find files
--  * <leader>fr show recent files
--  * <leader>fb Foobar
-- we'll document:
--  * <leader>fn new file
--  * <leader>fe edit file
-- and hide <leader>1

wk.register({
  f = {
    name = "file", -- optional group name
    f = { "<cmd>Telescope find_files<cr>", "Find File" }, -- create a binding with label
    r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File", noremap=false, }, -- additional options for creating the keymap
    g = { "<cmd>Telescope live_grep<cr>", "ripGreping through files" }, -- Grep through those files 💨
    --n = { "New File" }, -- just a label. don't create any mapping
    --e = "Edit File", -- same as above
    --["1"] = "which_key_ignore",  -- special label to hide it in the popup
    --b = { function() print("bar") end, "Foobar" } -- you can also pass functions!
  },
  g = {
    name = "go to...",
    d = { "<cmd>Telescope lsp_definitions<cr>", "definition" },
    i = { "<cmd>Telescope lsp_implementations<cr>", "implementation" },
    r = { "<cmd>Telescope lsp_references<cr>", "references" },
  }
}, { prefix = "<leader>" })

wk.register({
  -- Move with CTRL + hjkl around 🧠
  ["<C-h>"] = { "<cmd>wincmd h<cr>", "Move left" },
  ["<C-j>"] = { "<cmd>wincmd j<cr>", "Move down" },
  ["<C-k>"] = { "<cmd>wincmd k<cr>", "Move up" },
  ["<C-l>"] = { "<cmd>wincmd l<cr>", "Move right" },

  -- Some nice keybinds, inspired by https://lsp-zero.netlify.app/docs/language-server-configuration.html
  -- Test those with `:lua vim.lsp.buf.code_action()`
  ["K"] = { 
    function()
      vim.lsp.buf.hover()
    end,
    "Show info from LSP"
  },
  ["<F2>"] = { 
    function()
      vim.lsp.buf.rename()
    end,
    "Rename via LSP"
  },
  ["<F4>"] = { 
    function()
      vim.lsp.buf.code_actions()
    end,
    "Do some code actions"
  },
  ["<C-I>"] = { 
    function()
      vim.lsp.buf.format()
    end,
    "Format via LSP"
  },
})

