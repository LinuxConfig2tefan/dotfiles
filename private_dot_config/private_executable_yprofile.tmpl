#!/bin/zsh

# Keep ~ clean
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_BIN_HOME="$HOME/.local/bin" # env var à la 2tefan; location for all user executables

export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"

# Are we running wayland (sway) or x11 (i3)?
#export DISPLAY_SERVER=$(loginctl show-session $(awk '/tty/ {print $1}' <(loginctl)) -p Type | awk -F= '{print $2}')
if [[ "$XDG_CURRENT_DESKTOP" == "i3" || "$XDG_SESSION_DESKTOP" == "i3" ]]; then
	export DISPLAY_SERVER="x11"
elif [[ "$XDG_CURRENT_DESKTOP" =~ sway.* || "$XDG_SESSION_DESKTOP" =~ sway.* ]]; then
	export DISPLAY_SERVER="wayland"
	export MOZ_ENABLE_WAYLAND="1"
	export XDG_CURRENT_DESKTOP="$XDG_SESSION_DESKTOP"
else
	#echo "Missing XDG_CURRENT_DESKTOP, assuming x11"
	export DISPLAY_SERVER="x11"
fi


export ANDROID_SDK_ROOT="$HOME/dev/android/sdk"
export CHROME_EXECUTABLE="/usr/bin/chromium"
#export STUDIO_VM_OPTIONS="${XDG_DATA_HOME:-$HOME/.local/share}/android/VMOptions"
#export STUDIO_PROPERTIES="${XDG_DATA_HOME:-$HOME/.local/share}/android/StudioProperties"
#export STUDIO_JDK="${XDG_DATA_HOME:-$HOME/.local/share}/android/StudioJDK"
#export ANDROID_EMULATOR_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/android/Emulator"
export GRADLE_USER_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/gradle"
#export ANDROID_SDK_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/android"
#export STUDIO_PROPERTIES="${XDG_CONFIG_HOME:-$HOME/.config}/idea/idea.properties"

#export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
#export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"
export VSCODE_EXTENSIONS="${XDG_DATA_HOME:-$HOME/.local/share}/vscode-oss-extensions"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export CARGO_TARGET_DIR="$HOME/dev/rust/target"
export GNUPGHOME="${XDG_DATA_HOME:-$HOME/.local/share}/gnupg"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/npm/npmrc"
export VOLTA_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/volta"
export CUDA_CACHE_PATH="${XDG_CACHE_PATH:-$HOME/.cache}/nv"


# Set default stuff
export SUDO_ASKPASS={{ if ne .purpose "work" }}"/usr/lib/seahorse/ssh-askpass"{{ else }}"/usr/bin/ssh-askpass"{{ end }}
export QT_STYLE_OVERRIDE="kvantum"
#export QT_AUTO_SCREEN_SCALE_FACTOR=0
#export GTK_THEME="Arc-Dark-solid"

export BROWSER=firefox
export EXPLORER=thunar
export MAIL_CLIENT=evolution
export PERSONAL_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/2tefan"
export PERSONAL_SCRIPTS="${PERSONAL_CONFIG}/Scripts"

export PATH="${XDG_DATA_HOME:-$HOME/.local/share}/.node_modules:$PERSONAL_SCRIPTS:$PATH"
#export npm_config_prefix="${XDG_DATA_HOME:-$HOME/.local/share}/.node_modules"

# Setup wine env
source $PERSONAL_SCRIPTS/setup-wine-64.sh

# Xresources
if [[ ! -o login && "$DISPLAY_SERVER" == "x11" ]]; then
	xrdb ${XDG_CONFIG_HOME:-$HOME/.config}/Xresources
fi
