#!/usr/bin/env bash

readonly SEPERATOR=' // '
readonly QDBUS_SERVICE_NAME="org.kde.kdeconnect"
readonly QDBUS_DEVICE_PREFIX="/modules/kdeconnect/devices"
readonly QDBUS_METHODE_DEVICE_PREFIX="org.kde.kdeconnect.device"

readonly ICON_SMARTPHONE=''
readonly ICON_DESKTOP=''

send_qdbus() {
	device_id="$1"
	method="$2"

	if [ -z "$3" ]; then
		device_suffix=""
	else
		device_suffix="/$3"
	fi

	qdbus "$QDBUS_SERVICE_NAME" "$QDBUS_DEVICE_PREFIX/${device_id}${device_suffix}" "$method"
}

send_qdbus_device() {
	device_id="$1"
	method="$2"
	send_qdbus "$1" "$QDBUS_METHODE_DEVICE_PREFIX.$method"
}

send_qdbus_device_battery() {
	device_id="$1"
	method="$2"
	send_qdbus "$1" "$QDBUS_METHODE_DEVICE_PREFIX.$method" "battery"
}

send_qdbus_device_notifications() {
	device_id="$1"
	method="notifications.activeNotifications"
	send_qdbus "$1" "$QDBUS_METHODE_DEVICE_PREFIX.$method" "notifications"
}

add_smartphone() {
	battery_charge=$(send_qdbus_device_battery "$device_id" "battery.charge")
	is_charging=$(send_qdbus_device_battery "$device_id" "battery.isCharging")
	notifications=$(send_qdbus_device_notifications "$device_id")
	count_notifications=0

	echo -n "${ICON_SMARTPHONE} ${battery_charge}% "

	if [ -n "$notifications" ]; then
		readarray -t notifications_arr <<<"$notifications"
		count_notifications=${#notifications_arr[@]}
	fi

	#if [ "$battery_charge" -lt 20 ]; then
	#	battery_charge="%{F#f00}$battery_charge%{F-}"
	#fi

	if [ "$is_charging" = "true" ]; then
		echo -n "󱐥 "
	fi

	echo -n "- $count_notifications "

	if [ "$count_notifications" -gt 0 ]; then
		echo -n ""
	else
		echo -n ""
	fi

	echo
}

add_desktop() {
	echo "$ICON_DESKTOP"
}

add_device() {
	device_id="$1"

	case $(send_qdbus_device "$device_id" "type") in
		smartphone | phone)
			add_smartphone
			;;
		desktop)
			add_desktop
			;;
		*)
			echo "unknown device type"
			;;
	esac

}


devices=()

for device_id in $(qdbus "$QDBUS_SERVICE_NAME" /modules/kdeconnect org.kde.kdeconnect.daemon.devices); do
	is_reachable=$(send_qdbus_device "$device_id" isReachable)
	is_trusted=$(send_qdbus_device "$device_id" isTrusted)

	if [ "$is_trusted" = "false" ]; then
		continue
	fi

	if [ "$is_reachable" = "false" ]; then
		continue
	fi

	devices+=("$(add_device "$device_id")")
done

if [ ${#devices[@]} -eq 0 ]; then
	echo "No device connected"
else
	printf -v joined "%s$SEPERATOR" "${devices[@]}"
	echo "${joined%"$SEPERATOR"}"
fi

