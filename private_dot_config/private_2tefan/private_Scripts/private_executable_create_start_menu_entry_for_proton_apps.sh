#!/bin/sh

set -eu

: "${PATH_TO_APP:="$(realpath "${1}")"}"
: "${NAME:=$(basename "${PATH_TO_APP}" .exe) (Proton)}"

echo PATH_TO_APP="${PATH_TO_APP}"
echo NAME="${NAME}"

if [ ! -f "${PATH_TO_APP}" ]
then
    echo "'${PATH_TO_APP}' does not exist..."
    exit 1
fi

echo "[Desktop Entry]
Name=${NAME}
Exec=run_with_proton.sh \"${PATH_TO_APP}\"
Type=Application
Terminal=true
TerminalOptions=\s--noclose
StartupNotify=true" > "${XDG_DATA_HOME}/applications/proton/${NAME}.desktop"

