#!/bin/sh

set -eu

. $PERSONAL_CONFIG/env.sh

xrandr --output "$MONITOR_SOUND" --mode 1920x1080 --left-of "$MONITOR_1"
pactl set-card-profile alsa_card.pci-0000_0a_00.1 pro-audio
