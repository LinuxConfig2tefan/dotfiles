#!/bin/sh

set -eu

for dir in *; do current_dir=$(pwd) && cd $dir && git pull ; cd $current_dir ; done
