#!/bin/sh
# src: https://gist.githubusercontent.com/thingsiplay/3a933f557277906dc6b0e03ec8df5dbd/raw/b406b32604dfb63b83d80f5bbaafd80d09f69822/proton
# Execute Windows programs with Proton from Steams installation folder, without
# starting Steam client.
#
# 1. Create a directory for Proton environment to run in.  As an example make a
#    folder "proton" in your home directory.  This folder must exist in order
#    to make Proton work.
#
# 2. Point the variable "env_dir" in this script to that folder or...
#
# 3. ... alternatively set the environmenal variable "$PROTONPREFIX" to this
#    folder before running the script.  It works similar to the "$WINEPREFIX"
#    from WINE and will have higher priority over "env_dir".
#
# 4. Look in your Steam installation folder at "steamapps/common/" folder for
#    available Proton versions.  Pick one and point the script variable
#    "proton_version" to this that folder name, in example "Proton 3.16".
#    Note: You have to download a Proton version from Steam first, if none is
#    there yet.
#
# 5. Or alternatively set the environmental variable "$PROTONVERSION" to that
#    folder name of Proton version before running the script.  It has higher
#    priority over script variable "proton_version".
#
# 6. Optionally install/copy this script in a directory that is in your $PATH,
#    so you can run it easily from any place.  Or set the default interpreter
#    for .exe files to this script.
#
# Usage:
#   proton program.exe
#
# or:
#   export PROTONPREFIX="$HOME/proton_316"
#   export PROTONVERSION="Proton 3.16"
#   proton program.exe

. "${PERSONAL_CONFIG}"/env.sh

# Folder name of the Proton version found under "steamapps/common/".
: "${PROTON_VERSION:=Proton 7.0}"

# Path to installation directory of Steam.
# Alternate path: "$HOME/.steam/steam"
: "${CLIENT_DIR:=$XDG_DATA_HOME/Steam}"

# Default data folder for Proton/WINE environment.  Folder must exist.
# If the environmental variable PROTONPREFIX is set, it will overwrite env_dir.
: "${ENV_DIR:=$WINDOWS_ENVS/proton}"
if [ ! -f "${ENV_DIR}" ]; then
	mkdir -p "${ENV_DIR}"
fi


# Proton modes to run
#   run = start target app
#   waitforexitandrun = wait for wineserver to shut down
#   getcompatpath = linux -> windows path
#   getnativepath = windows -> linux path
mode=run

if [ ! -f "${1}" ]; then
	echo "'${1}' does not exist..."
	exit 1
fi

# EXECUTE
export STEAM_COMPAT_CLIENT_INSTALL_PATH="${CLIENT_DIR}"
export STEAM_COMPAT_DATA_PATH="${ENV_DIR}"
"${CLIENT_DIR}/steamapps/common/${PROTON_VERSION}/proton" "${mode}" "$*"

