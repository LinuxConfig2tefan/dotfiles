#!/bin/bash
# Example: set_vlan_on_int.sh enp6s0 9 192.168.89.5/24

set -aeux

interface="$1"
vlan_id="$2"
ip="$3"

new_int_name="${interface}.${vlan_id}"

sudo ip link add link "$interface" name "$new_int_name" type vlan id "${vlan_id}"
sudo ip link set dev "$new_int_name" up 
sudo ip addr add "$ip" dev "$new_int_name"

