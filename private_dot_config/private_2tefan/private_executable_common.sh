#!/bin/sh

custom_mount() {
	name=$1
	username=${2:-"stefan"}
	identityFile=${3:-$name}
	host=${4:-$name.lan}
	sudo mkdir "/run/media/$USER/$name" -p
	sudo chown "$USER:$USER" "/run/media/$USER/$name" -R
	sshfs "$username@$host:/" "/run/media/$USER/$name" -o "IdentityFile=~/.ssh/$identityFile"
}

custom_mount_without_host() {
	name=$1
	identityFile=${2:-$name}
	sudo mkdir "/run/media/$USER/$name" -p
	sudo chown "$USER:$USER" "/run/media/$USER/$name" -R
	sshfs "$name:/" "/run/media/$USER/$name" -o "IdentityFile=~/.ssh/$identityFile"
}

startup() {
	program=$1
	killall "$program"
	$program &
}

start_if_not_running() {
	program="$1"
	location="$2"

	ps -aux | grep "$program" | grep -v "grep"
	if [ $? -eq 0 ]; then
		echo "'${program}' already running..."
	else
		if [ -z "$location" ]; then
			$program &
		else
			$location &
		fi
	fi
}

start_if_not_running_flatpak() {
	program=$1

	flatpak ps | grep "$program"
	if [ $? -eq 0 ]; then
		echo "'${program}' already running..."
	else
		flatpak run "$program"
	fi
}

prepare_wine_env() {
	bit="$1"
	export WINEPREFIX="${WINDOWS_ENVS}/wine_${bit}"
	export WINEARCH="win${bit}"
}

setup_vnc() {
	destination_address=$1
	destination_username=$2
	identity_file=$3

	destination_port_ssh=${4:-22}
	destination_port_vnc=${5:-5901}
	source_port_vnc=${6:-5901}

	echo "Please connect VNC client to localhost:$source_port_vnc"

	ssh "$destination_address" \
		-l "$destination_username" \
		-p "$destination_port_ssh" \
		-i "$identity_file" \
		-L "$destination_port_vnc:localhost:$source_port_vnc"

	echo "Done"
}

check_if_number() {
	case $1 in
		'' | *[!0-9]*)
			echo "Not a number ($1)..." 1>&2
			exit 1
			;;
	esac
}

add_custom_xrandr_mode() {
	montior="$1"
	mode_name="1680x1050_60.00"

	xrandr --newmode "$mode_name" 146.25 1680 1784 1960 2240  1050 1053 1059 1089 -hsync +vsync
	xrandr --addmode "$montior" "$mode_name"
}

turn_on_monitors() {
	parameters=""

	for monitor_number in "$@"; do
		#eval $name_of_variable="simpleword"
		monitor_setup_name=MONITOR_${monitor_number}_SETUP
		eval monitor_setup="\${$monitor_setup_name}"
		if [ -n "$monitor_setup" ] && [ "$monitor_setup" != "todo" ]; then
			parameters="$parameters $monitor_setup"

			if [ "$DISPLAY_SERVER" = "wayland" ]; then
				parameters="$parameters --on"
			fi
		fi
	done

	if [ "$DISPLAY_SERVER" = "x11" ]; then
		xrandr $parameters
	elif [ "$DISPLAY_SERVER" = "wayland" ]; then
		wlr-randr $parameters
	fi
}

turn_off_monitors() {
	parameters=""

	for monitor_number in "$@"; do
		#eval $name_of_variable="simpleword"
		monitor_name=MONITOR_${monitor_number}
		eval monitor_setup="\${$monitor_name}"

		if [ -n "$monitor_setup" ] && [ "$monitor_setup" != "todo" ]; then
			parameters="$parameters --output $monitor_setup --off"
		fi
	done

	if [ "$DISPLAY_SERVER" = "x11" ]; then
		xrandr $parameters
	elif [ "$DISPLAY_SERVER" = "wayland" ]; then
		wlr-randr $parameters
	fi
}

get_screen_width() {
	swaymsg -t get_outputs -r | jq ".[0].current_mode.width / .[0].scale"
}

restart_infobar() {
	if [ "$DISPLAY_SERVER" = "x11" ]; then
		killall polybar
		"$XDG_CONFIG_HOME/polybar/launch.sh"
	elif [ "$DISPLAY_SERVER" = "wayland" ]; then
		killall waybar
		wb_configs="$XDG_CONFIG_HOME/waybar"

		waybar --config "$wb_configs/config-laptop.jsonc" &
		
		if [ "$(get_screen_width)" -gt 1500 ] && nc -vz jet.lan 6600; then
			killall waybar
			waybar --config "$wb_configs/config-home.jsonc" &
		fi
	fi
}   

redraw_background() {
	"$PERSONAL_SCRIPTS/randomize-background"
}

reload_desktop() {
	restart_infobar
	redraw_background
}

